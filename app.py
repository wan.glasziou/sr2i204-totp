# importing needed libraries
import pyotp
from flask import *
from flask_bootstrap import Bootstrap

# configuring flask application
app = Flask(__name__)
app.config["SECRET_KEY"] = "APP_SECRET_KEY"
Bootstrap(app)

totp_token = pyotp.random_base32() # created only once


# login page route
@app.route("/")
def login():
    return render_template("login.html")


# login form route
@app.route("/", methods=["POST"])
def login_form():
    username_db = ["Erwan", "Clara"]
    password_db = ["password", "theBest"]

    # getting form data
    username = request.form.get("username")
    password = request.form.get("password")

    if username in username_db:
        pos = username_db.index(username)
        if password == password_db[pos]:
            return redirect(url_for("login_2fa"))
        else:
            return redirect(url_for("login"))
    else:
        # inform users if credentials are invalid
        return redirect(url_for("login"))
    
    
# 2FA page route
@app.route("/login/2fa/")
def login_2fa():
    # generating random secret key for authentication
    # secret = pyotp.random_base32()
    return render_template("login_2fa.html", secret=totp_token)


# 2FA form route
@app.route("/login/2fa/", methods=["POST"])
def login_2fa_form():
    # getting secret key used by user
    totp_token = request.form.get("secret")
    # getting OTP provided by user
    otp = request.form.get("otp")

    # verifying submitted OTP with PyOTP
    if pyotp.TOTP(totp_token).verify(otp):
        # inform users if OTP is valid
        flash("The TOTP 2FA token is valid", "success")
        return redirect(url_for("login_2fa"))
    else:
        # inform users if OTP is invalid
        flash("You have supplied an invalid 2FA token!", "danger")
        return redirect(url_for("login_2fa"))


# running flask server
if __name__ == "__main__":
    app.run(debug=True)
