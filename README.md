# sr2i204-TOTP

SR2I-204 Project : Implementation of TOTP on a website for authentification.

## Name

sr2i204-TOTP

## Description
Implementation of the TOTP algorithm on a website for authentification in the context of the SR2I-204 class at Telecom Paris.

## Roadmap
- Create the website
- Add connexion form and supporting database
- Add TOTP authentication
- Add form to create account

## Authors and acknowledgment
Authors : Glasziou Erwan, Delabrouille Clara.

## Project status
Research Phase
Development phase - ongoing

## Bibliography
- https://datatracker.ietf.org/doc/html/rfc6238
- https://en.wikipedia.org/wiki/Time-based_One-Time_Password
- https://en.wikipedia.org/wiki/HMAC-based_one-time_password
- https://flask.palletsprojects.com/en/2.0.x/ (Use Flask to create the web page / server)
- https://developer.mozilla.org/fr/docs/Learn/Forms/Your_first_form (Create the authentication form)
- https://fr.wikipedia.org/wiki/HMAC (HMAC algorithm, used for OTP code generation)
- https://github.com/pyauth/pyotp/tree/6568c1a83af8e0229f3c4b28d03552d601e2b7fe/src/pyotp (pyotp Python library source code)

## Description of the project
We implemented a basic authentication form on the first page of the app (localhost:5000 when app.py runs).

<img src="login_page.PNG" alt="login page" width="500"/>


When correctly authenticated (two users are registered : "Erwan" ("password") and "Clara" ("theBest")), we are redirected to the TOTP authentication page.

<img src="TOTP_page.PNG" alt="TOTP page" width="500"/>


The app generates a token once for the user, that is when the app is launched, with the command `pyotp.random_base32()`. We then copy the provided token. We can generate the OTP code with Python on our computer.

<img src="code_otp.PNG" alt="OTP code generation" width="500"/>


The generation of the OTP code is based on the time :
- the time is provided as an integer to the `generate_otp` function
- the time is converted to bytestring format
- the token is provided as a string
- the token is converted to bytes
- from these, a hmac object is generated
- we obtain a hashed version of the token `hmac_hash`
- an offset is then computed as `hmac_hash[-1] & 0xf`
- by sliding the bits of hmac_hash according to the offset and adding them, a code is generated
- after another mathematical operation, conversion to string and padding until the number of required digits is reached, the code is returned

We can then paste it into the empty field on the TOTP page (the code disappears after submission). A message flashes, informing the user whether the code filled in is correct...

<img src="valid_otp.PNG" alt="Correct code" width="500"/>


... or not.

<img src="wrong_otp.PNG" alt="Wrong code" width="500"/>
